/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  scanner
  \brief    Declaration of class oMatrix
*/

#ifndef OMATRIX_HH
#define OMATRIX_HH 1

#include "gVariables.hh"
#include "sOutputManager.hh"


/*!
  \class   oMatrix
  \brief   Structure designed for basic matrices operations.
  \details This structure is mostly used in scanner classes, for geometrical rotation purposes
*/
class oMatrix
{
  // Constructor & Destructor
  public:
    /*!
      \brief   oMatrix constructor. 
               Initialize the member variables to their default values.
    */
    oMatrix();
    /*!
      \brief   oMatrix destructor. 
               Free memory of the oMatrix object.
    */
    ~oMatrix();
    /*!
      \param   nl : a number of lines
      \param   nc : a number of colons
      \brief   oMatrix constructor. 
               Instanciate a matrix structure with the number of lines and colons provided in parameters
    */
    oMatrix(uint16_t nl, uint16_t nc);


  // -------------------------------------------------------------------
  // Public member functions
  public:
    /*!
      \fn      void oMatrix::Allocate()
      \param   nl: a number of lines
      \param   nc: a number of colons
      \brief   Instanciate a Matrix structure with the number of lines and colons provided in parameters
    */
    void Allocate(uint16_t nl, uint16_t nc);
    /*!
      \fn      int oMatrix::SetMatriceElt()
      \param   l: a line index
      \param   c: a colon index
      \param   a_val : a value to initialize the matrix element with
      \brief   Set the matrix element corresponding to the argument indices with the provided value.
      \return  0 if success, positive value otherwise
    */
    int SetMatriceElt(uint16_t l, uint16_t c, FLTNBLUT a_val);
    /*!
      \fn      FLTNBLUT oMatrix::GetMatriceElt()
      \param   l: a line index
      \param   c: a colon index
      \return  the matrix element value corresponding to the argument indices provided in arguments.
      \todo    return error if unmatching number of lines/colons with the matrix initialization ?
    */
    FLTNBLUT GetMatriceElt(uint16_t l,uint16_t c);
    /*!
      \fn      int oMatrix::Multiplication()
      \param   ap_Mtx: a line index
      \param   ap_MtxResult: a colon index
      \brief   Multiply the member matrix with the matrix provided in 1st parameter
               Return the result in the matric provided in 2nd parameter
      \return  0 if success, positive value otherwise
    */
    int Multiplication(oMatrix *a_Mtx, oMatrix *a_MtxResult);


  // -------------------------------------------------------------------
  // Data members. (Nb lines/colons set public in order to optimize Multiplication function)
  public:
    uint16_t m_lin;     /*!< Number of lines in the matrix. Default =0 */
    uint16_t m_col;     /*!< Number of colons in the matrix. Default =0 */
  private:
    FLTNBLUT **m2p_Mat; /*!< 2D pointer containing the matrix elements. Default =NULL */
};

#endif
