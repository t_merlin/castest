/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  scanner
  \brief    Declaration of class iScannerPET
*/

#ifndef ISCANNERPET_HH
#define ISCANNERPET_HH 1

#include "gVariables.hh"
#include "vScanner.hh"
#include "sAddonManager.hh"


/*!
  \class   iScannerPET
  \brief   This class is used to represent any cylindrical PET scanner
  \details Inherits from vScanner
*/
class iScannerPET : public vScanner
{
  // Constructor & Destructor
  public:
    /*!
      \brief   iScannerPET constructor. 
               Initialize the member variables to their default values.
    */
    iScannerPET();

    /*!
      \brief   iScannerPET destructor. 
    */
    ~iScannerPET();


  // -------------------------------------------------------------------
  // Public member functions
  public:
    // Function for automatic insertion (put the class name as the parameters and do not add semi-colon at the end of the line)
    FUNCTION_SCANNER(iScannerPET)
    /*!
      \fn      int iScannerPET::Instantiate()
      \param   a_scannerFileIsLUT : boolean indicating if the file describing 
                                   the system is a generic file (0) or custom Look-up-table (1)
      \brief   Get mandatory informations from the scanner file 
               and allocate memory for the member variables
      \return  0 if success, positive value otherwise
      \todo    initialization of m_maxRingDiff for scanners with several crystal layers
    */
    int Instantiate(bool a_scannerFileIsLUT);
    /*!
      \fn      int iScannerPET::CheckParameters()
      \brief   Check if all parameters have been correctly initialized
      \return  0 if success, positive value otherwise
    */
    int CheckParameters();
    /*!
      \fn      int iScannerPET::Initialize()
      \brief   Check general initialization and set several parameters to their default value
      \return  0 if success, positive value otherwise
    */
    int Initialize();
    /*!
      \fn      int iScannerPET::BuildLUT()
      \param   a_scannerFileIsLUT : boolean indicating if the file describing 
                                    the SPECT camera is a generic file (0) or custom Look-up-table (1)
      \brief   Call the functions to generate the LUT or read the user-made LUT depending on the user choice
      \return  0 if success, positive value otherwise
      \todo    default values to max ring diff
    */
    int BuildLUT(bool a_scannerFileIsLUT);
    /*!
      \fn      int iScannerPET::GetPositionsAndOrientations()
      \param   a_index1 : index of the 1st crystal 
      \param   a_index2 : index of the 2nd crystal 
      \param   ap_Position1[3] : x,y,z cartesian position of center of the 1st crystal
      \param   ap_Position2[3] : x,y,z cartesian position of center of the 2nd crystal
      \param   ap_Orientation1[3] : x,y,z components of the orientation vector related to the 1st crystal
      \param   ap_Orientation2[3] : x,y,z components of the orientation vector related to the 2nd crystal
      \param   ap_POI1 : x,y,z components of the Point Of Interation related to the 1st crystal
      \param   ap_POI2 : x,y,z components of the Point Of Interation related to the 2nd crystal
      \brief   Get the central positions and orientations of the scanner elements from their indices.
      \details This method is very general and is used by the vProjector. 
               From these positions and orientations, other methods can be used by specific projectors to get specific positions.
               Position calculations include POI and mean depth of interaction
      \todo    some cases depending on POI are not implemented
      \return  0 if success, positive value otherwise
    */
    int GetPositionsAndOrientations( int a_index1, int a_index2,
                                     FLTNB ap_Position1[3], FLTNB ap_Position2[3],
                                     FLTNB ap_Orientation1[3], FLTNB ap_Orientation2[3],
                                     FLTNB* ap_POI1 = NULL, FLTNB* ap_POI2 = NULL );
    /*!
      \fn      int iScannerPET::GetRdmPositionsAndOrientations()
      \param   a_index1 : index of the 1st crystal 
      \param   a_index2 : index of the 2nd crystal 
      \param   ap_Position1[3] : x,y,z cartesian position of center of the 1st crystal
      \param   ap_Position2[3] : x,y,z cartesian position of center of the 2nd crystal
      \param   ap_Orientation1[3] : x,y,z components of the orientation vector related to the 1st crystal
      \param   ap_Orientation2[3] : x,y,z components of the orientation vector related to the 2nd crystal
      \brief   Get random positions of the scanner elements and their orientations from their indices.
      \details - Find the scanner elements described by the two indexes passed in parameters. 
      \details - Compute random positions inside the elements described by the indexes passed in parameters
      \details - Find the scanner elements described by the two indexes passed in parameters.
      \details - Write the corresponding random cartesian coordinates in the positions parameters.
      \details Position calculations include POI and mean depth of interaction
      \todo    fix the possibility to draw LOR outside the actual crystal volume (if mp_meanDepthOfInteraction != 0.5)
      \todo    some cases depending on POI are not implemented
      \return  0 if success, positive value otherwise
    */
    int GetRdmPositionsAndOrientations( int a_index1, int a_index2,
                                        FLTNB ap_Position1[3], FLTNB ap_Position2[3],
                                        FLTNB ap_Orientation1[3], FLTNB ap_Orientation2[3] );
    /*!
      \fn      int iScannerPET::GetPositionWithRandomDepth()
      \param   a_index1 : index of the 1st crystal
      \param   a_index2 : index of the 2nd crystal
      \param   ap_Position1[3] : x,y,z cartesian position of the point related to the 1st index (see child function for more details)
      \param   ap_Position2[3] : x,y,z cartesian position of the point related to the 2st index (see child function for more details)
      \brief   Get the positions and orientations of scanner elements from their indices, with a random depth.
      \details Method for testing purposes. Does not include POI and mean depth of interaction
      \return  0 if success, positive value otherwise
    */
    int GetPositionWithRandomDepth( int a_index1, int a_index2, FLTNB ap_Position1[3], FLTNB ap_Position2[3] );
    /*!
      \fn      int iScannerPET::GetTwoCorners()
      \param   a_index1 : index of the 1st crystal 
      \param   a_index2 : index of the 2nd crystal
      \param   ap_CornerInf1[3]
      \param   ap_CornerSup1[3]
      \param   ap_CornerInf2[3]
      \param   ap_CornerSup2[3]
      \brief   Get the cartesian coordinaters of the two opposite corners of a scanner element.
      \todo    Not implemented yet 
      \return  0 if success, positive value otherwise
    */
    int GetTwoCorners( int a_index1, int a_index2,
                       FLTNB ap_CornerInf1[3], FLTNB ap_CornerSup1[3],
                       FLTNB ap_CornerInf2[3], FLTNB ap_CornerSup2[3] );
    /*!
      \fn      inline int iScannerPET::GetSystemNbElts()
      \return  the number of crystals in the PET system
    */
    inline int GetSystemNbElts()
           {return m_nbCrystals;};
    /*!
      \fn      inline int iScannerPET::GetScannerLayerNbRings()
      \param   a_layer
      \brief   Return the number of rings for a specific crystal layer
      \return  the number rings for the layer, or -1 (error) if the layer
               index not consistent with the nb of layer in the system
    */
    inline int GetScannerLayerNbRings(int a_layer)
           {return (a_layer<m_nbLayers) ? mp_nbRings[a_layer] : -1;}
    /*!
      \fn      int iScannerPET::IsAvailableLOR()
      \param   a_elt1 : index of the 1st crystal
      \param   a_elt2 : index of the 2nd crystal
      \brief   Check if the LOR formed by the crystalf whose indices are passed 
               in parameters is available according to the scanner restrictions
      \details This function is dedicated to analytic projection and list-mode sensitivity image generation \n
               The PET implementation checks the LOR availability according to the minimal (transaxial) angle difference 
               and the maximal ring difference between two crystals
      \todo    min angle difference (currently just system dependent) may be estimated from the FOV requested by the user ?
      \todo    Restriction for ring_ID NOT safe for user using their own LUT !!! \n
      #         Perhaps throw warning in this case
      \return  1 if the LOR is available, 0 otherwise
    */
    int IsAvailableLOR(int a_elt1, int a_elt2);
    /*!
      \fn      int iScannerPET::GetGeometricInfoFromDatafile()
      \brief   Retrieve PET geometric informations from the datafile
      \details Recover the (axial) max ring difference
      \return  0 if success, positive value otherwise
    */
    int GetGeometricInfoFromDatafile(string a_pathToDataFilename);
    /*!
      \fn      inline int iScannerPET::SetPETMaxRingDiff()
      \param   a_maxRingDiff
      \brief   Set the maximal ring difference (if any) 
    */
    inline int SetPETMaxRingDiff(int a_maxRingDiff)
           {m_maxRingDiff = a_maxRingDiff; return 0;}
    /*!
      \fn      void iScannerPET::ShowHelp()
      \brief   Display help
      \todo    Provide informations about PET system initialization ?
    */
    void ShowHelp();


  // -------------------------------------------------------------------
  // Functions dedicated to Analytic Projection
    /*!
      \fn      int iScannerPET::PROJ_GetPETSpecificParameters()
      \param   ap_maxRingDiff
      \brief   Set pointers passed in argument with the related PET specific variables
               This function is used to recover these values in the datafile object
      \return  0 if success, positive value otherwise
    */
    int PROJ_GetPETSpecificParameters(int* ap_maxRingDiff);


  // -------------------------------------------------------------------
  // Private member functions
  private:
    /*!
      \fn      int iScannerPET::LoadLUT()
      \brief   Load a precomputed scanner LUT
      \details Read mandatory data from the header of the LUT. \n
               Then load the LUT elements for each crystal
      \return  0 if success, positive value otherwise
    */
    int LoadLUT();
    /*!
      \fn      int iScannerPET::ComputeLUT()
      \brief   Compute the LUT of the scanner from a generic (.geom) file
      \details Read mandatory data from the geom file. Then compute the LUT elements for each crystal from the geometry described in the file
      \details Compute the look-up-tables of the system containing the locations of the scanner elements center in space and their orientations
      \todo    Add option to inverse rsector if transaxial orientation is counter-clockwise. ?
      \todo    rotation for non-perfectly cylindric scanner (angles along the Z axis)
      \return  0 if success, positive value otherwise
    */
    int ComputeLUT();
    /*!
      \fn      int iScannerPET::GetLayer()
      \param   a_idx : index of the crystal in the loaded LUT
      \brief   Get the layer from which the 'a_index' crystal belongs to
      \return  layer index
    */
    int GetLayer(int a_idx);
    /*!
      \fn      int iScannerPET::GetRingID()
      \param   a_idx : index of the crystal in the loaded LUT
      \brief   Get the ring which the #a_idx crystal belongs to
      \todo    /!\ Current implementation only works with scanner whose elements are ring-indexed 
      \todo    Need to find a solution to deal with potential geometry containing several layers of crystals with different number of rings \n
               We could use the geometrical position of each elements to get this information,
               however maybe too complicated for potential scanners with non-standard geometry
      \return  layer index
    */
    int GetRingID(int a_idx);


  // -------------------------------------------------------------------
  // Data members
  private:
    int m_nbLayers;                    /*!< Number of crystal layers in the scanner */
    int m_nbCrystals;                  /*!< Total number of crystal in the scanner */
    int* mp_nbRings;                   /*!< Number of rings (for each layer) */
    int* mp_nbCrystalsInLayer;         /*!< Number of crystals (for each layer) */
      
    FLTNB* mp_crystalCentralPositionX; /*!< Cartesian coordinate on X-axis of the center of each crystal*/
    FLTNB* mp_crystalCentralPositionY; /*!< Cartesian coordinate on Y-axis of the center of each crystal*/
    FLTNB* mp_crystalCentralPositionZ; /*!< Cartesian coordinate on Z-axis of the center of each crystal*/

    FLTNB* mp_crystalOrientationX;     /*!< X-axis orientation of each crystal*/
    FLTNB* mp_crystalOrientationY;     /*!< Y-axis orientation of each crystal*/
    FLTNB* mp_crystalOrientationZ;     /*!< Z-axis orientation of each crystal*/

    FLTNB* mp_sizeCrystalTrans;        /*!< Transaxial size of a crystal (for each layer)*/
    FLTNB* mp_sizeCrystalAxial;        /*!< Axial size of a crystal (for each layer)*/
    FLTNB* mp_sizeCrystalDepth;        /*!< Depth of a crystal (for each layer)*/
    FLTNB* mp_meanDepthOfInteraction;  /*!< Mean depth of interaction in a crystal (for each layer)*/
    
    FLTNB m_minAngleDifference;        /*!< Minimal transaxial angle difference for the system*/

    int m_maxRingDiff;                 /*!< Maximal ring difference for a specific acquisition*/
};


// Class for automatic insertion (set here the visible scanner type name, put the class name as the parameters and do not add semi-colon at the end of the line)
CLASS_SCANNER(PET,iScannerPET)

#endif
