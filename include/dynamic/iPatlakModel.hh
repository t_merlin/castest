/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  dynamic
  \brief    Declaration of class iPatlakModel
*/


#ifndef IPATLAKMODEL_HH
#define IPATLAKMODEL_HH 1

#include "vDynamicModel.hh"
#include "sAddonManager.hh"



/*!
  \class   iPatlakModel
  \brief   This class implements the Patlak model, to model kinetics of irreversible radiotracers
*/
class iPatlakModel : public vDynamicModel
{
  // -----------------------------------------------------------------------------------------
  // Constructor & Destructor
  public:
    /*!
      \fn      iPatlakModel::iPatlakModel
      \brief   Constructor of iPatlakModel. Simply set all data members to default values.
    */
    iPatlakModel();
    /*!
      \fn      iPatlakModel::~iPatlakModel
      \brief   Destructor of iPatlakModel
    */
    ~iPatlakModel();


  // -----------------------------------------------------------------------------------------
  // Public member functions related to the initialization of the model
  public:
    // Function for automatic insertion (put the class name as the parameters and do not add semi-colon at the end of the line)
    FUNCTION_DYNAMICMODEL(iPatlakModel)
    /*!
      \fn      iPatlakModel::CheckSpecificParameters
      \brief   This function is used to check whether all member variables
               have been correctly initialized or not.
      \return  0 if success, positive value otherwise.
    */
    int CheckSpecificParameters();
    /*!
      \fn      iPatlakModel::ReadAndCheckConfigurationFile
      \param   const string& a_configurationFile : ASCII file containing informations about a dynamic model
      \brief   This function is used to read options from a configuration file.
      \return  0 if success, other value otherwise.
    */
    int ReadAndCheckConfigurationFile(string a_fileOptions);
    /*!
      \fn      iPatlakModel::ReadAndCheckOptionsList
      \param   const string& a_optionsList : a list of parameters separated by commas
      \brief   This function is used to read parameters from a string.
      \return  0 if success, other value otherwise.
    */
    int ReadAndCheckOptionsList(string a_listOptions);
    /*!
      \fn      iPatlakModel::Initialize
      \brief   This function is used to initialize Patlak parametric images and basis functions
      \todo    Read Interfile for parametric images initialization
      \return  0 if success, other value otherwise.
    */
    int Initialize();
    /*!
      \fn      iPatlakModel::ShowHelp
      \brief   Print out specific help about the implementation of the Patlak
               model and its initialization
    */
    void ShowHelp();


  // -----------------------------------------------------------------------------------------
  // Public member functions called by the main iterative algorithm class
    /*!
      \fn      iPatlakModel::EstimateModelParameters
      \param   ap_ImageS : pointer to the ImageSpace
      \param   a_ite : index of the actual iteration (not used)
      \param   a_sset : index of the actual subset (not used)
      \brief   Estimate Patlak parametric images
      \return  0 if success, other value otherwise.
    */
    int EstimateModelParameters(oImageSpace* ap_Image, int a_ite, int a_sset);
    /*!
      \fn      iPatlakModel::FitModel
      \param   ap_ImageS : pointer to the ImageSpace
      \param   a_ite : index of the actual iteration (not used)
      \param   a_sset : index of the actual subset (not used)
      \brief   Estimate image using Patlak parametric images and basis functions
      \return  0 if success, other value otherwise.
    */
    int FitModel(oImageSpace* ap_Image, int a_ite, int a_sset);
    /*!
      \fn      iPatlakModel::SaveParametricImages
      \param   ap_ImageS : pointer to the ImageSpace
      \param   a_ite : index of the actual iteration
      \brief   Write parametric images on disk if 'm_savePImgFlag' is enabled
      \todo    Interfile management
      \return  0 if success, other value otherwise.
    */
    int SaveParametricImages(int a_iteration);


  // -----------------------------------------------------------------------------------------
  // Data members
  protected:
    FLTNB** m2p_parametricImages; /*!< Image matrix containing the Patlak parametric images \n
                                       2 pointers:  \n
                                       1: parametric image related to the Patlak slope(0) or intercept(1). \n
                                       2: 3D voxels */
    FLTNB** m2p_patlakTACs;       /*!< Vector containing the Patlak temporal basis functions \n
                                       2 pointers:  \n
                                       1: index of the temporal function (Patlak slope(0) or intercept(1)) \n
                                       2: coefficient of the functions for each time points of a dynamic acquisition */
    string m_fileOptions;         /*!<Path to a configuration file */
    string m_listOptions;         /*!<String containing a list of options */
    bool m_savePImgFlag;          /*!<Flag indicating if parametric images should be written on disk (default=true) */
};

// Class for automatic insertion (set here the visible dynamic model's name, put the class name as the parameters and do not add semi-colon at the end of the line)
CLASS_DYNAMICMODEL(PatlakModel,iPatlakModel)

#endif
