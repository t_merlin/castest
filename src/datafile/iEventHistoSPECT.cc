/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class iEventHistoSPECT

  - separators: X
  - doxygen: X
  - default initialization: X
  - CASTOR_DEBUG: none
  - CASTOR_VERBOSE: none
*/


/*!
  \file
  \ingroup datafile

  \brief Implementation of class iEventHistoSPECT
*/


#include "iEventHistoSPECT.hh"
#include "vDataFile.hh" //  DATA_MODE/ DATA_TYPE macros


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Constructor()
*/

iEventHistoSPECT::iEventHistoSPECT() : iEventSPECT()
{
  m_dataType = TYPE_SPECT;
  m_dataMode = MODE_HISTOGRAM;
}


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Destructor()
*/
iEventHistoSPECT::~iEventHistoSPECT() {}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*!
  \fn virtual void Describe()
  \brief This function can be used to get a description of the event printed out
*/
void iEventHistoSPECT::Describe()
{
  Cout("---------------------- iEventHistoSPECT::Describe() --------------------------" << endl);
  Cout("sizeof(FLTNB): " << sizeof(FLTNB) << endl);
  Cout("Time: " << m_timeInMs << " ms" << endl);
  Cout("Number of lines: " << m_nbLines << endl);
  for (uint16_t l=0; l<m_nbLines; l++) Cout("  --> ID1: " << mp_ID1[l] << " | ID2: " << mp_ID2[l] << endl);
  Cout("Scatter rate: " << m_eventScatRate << endl);
  Cout("Normalization factor: " << m_eventNormFactor << endl);
  Cout("----------------------------------------------------------------------------" << endl);
}
