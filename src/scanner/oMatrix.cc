/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class oMatrix

  - separators: X
  - doxygen: X
  - default initialization: X
  - CASTOR_DEBUG: none
  - CASTOR_VERBOSE: none
*/

/*!
  \file
  \ingroup  scanner
  \brief    Implementation of class oMatrix
*/

#include "oMatrix.hh"


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \brief oMatrix constructor. 
         Initialize the member variables to their default values.
*/
oMatrix::oMatrix() 
{
  m_lin = 0;
  m_col = 0;

  m2p_Mat = NULL;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \param nl : a number of lines
  \param nc : a number of colons
  \brief oMatrix constructor. 
         Instanciate a Matrix structure with the number of lines and colons provided in parameters
*/
oMatrix::oMatrix(uint16_t nl, uint16_t nc) 
{
  m_lin = nl;
  m_col = nc;
  m2p_Mat = new FLTNBLUT *[nl];

  for(uint16_t l=0 ; l<nl ; l++)
    m2p_Mat[l] = new FLTNBLUT[nc];
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \brief oMatrix destructor. 
         Free memory of the oMatrix object.
*/
oMatrix::~oMatrix() 
{
  for(uint16_t l=0 ; l<m_lin ; l++)
    if(m2p_Mat[l]) delete[] m2p_Mat[l];

  if(m2p_Mat) delete[] m2p_Mat;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn Allocate
  \param nl : a number of lines
  \param nc : a number of colons
  \brief Instanciate a Matrix structure with the number of lines and colons provided in parameters
*/
void oMatrix::Allocate(uint16_t nl, uint16_t nc) 
{
  // No verbosity in oMatrix structure. Show this only if CASTOR_DEBUG is enabled (for bug tracking)
  #ifdef CASTOR_DEBUG
  Cout("oMatrix::Allocate() ...");
  #endif
  
  // Free memory in case the matrix had already been allocated
  if(m2p_Mat != NULL)
  {
    for(uint16_t l=0 ; l<m_lin ; l++)
      if(m2p_Mat[l]) delete[] m2p_Mat[l];

    delete[] m2p_Mat;
  }
  
  m_lin = nl;
  m_col = nc;
  m2p_Mat = new FLTNBLUT *[nl];

  for(uint16_t l=0 ; l<nl ; l++)
    m2p_Mat[l] = new FLTNBLUT[nc];
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn SetMatriceElt
  \param l : a line index
  \param c : a colon index
  \param a_val : a value to initialize the matrix element with
  \brief set the matrix element corresponding to the argument indices with the provided value.
  \return 0 if success, positive value otherwise
*/
int oMatrix::SetMatriceElt(uint16_t l, uint16_t c, FLTNBLUT a_val) 
{
  if(l>=m_lin || c>=m_col)
  {
    Cerr("***** oMatrix::SetMatriceElt()-> Nb of (lin,col) ("<<l+1<<","<<c+1<<") in parameters ");
    Cerr("> to the number of (lin,col) of this matrix ("<<m_lin<<","<<m_col<<") !" << endl);
    return 1;
  }
  
  m2p_Mat[l][c] = a_val;
  
  return 0;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*!
  \fn GetMatriceElt
  \param l : a line index
  \param c : a colon index
  \return the matrix element value corresponding to the argument indices provided in arguments.
  \todo return error if unmatching number of lines/colons with the matrix initialization ?
*/
FLTNBLUT oMatrix::GetMatriceElt(uint16_t l,uint16_t c) 
{
  return m2p_Mat[l][c];
}
    
    
    
// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn Multiplication
  \param ap_Mtx : a line index
  \param ap_MtxResult : a colon index
  \brief Multiply the member matrix with the matrix provided in 1st parameter
         Return the result in the matric provided in 2nd parameter
  \return 0 if success, positive value otherwise
*/
int oMatrix::Multiplication(oMatrix *ap_Mtx, oMatrix *ap_MtxResult)
{  
  double sum=0;

  if ( m_col != ap_Mtx->m_lin ) 
  {
    Cerr("***** oMatrix::Multiplication()-> Not matching number of colons and lines of the two matrices !");
    return 1;
  }
  else if (ap_MtxResult == NULL)
  {
    Cerr("***** oMatrix::Multiplication()-> The resulting matrix has not been allocated !");
    return 1;
  }
  else
  {
    for ( uint16_t tl = 0; tl < m_lin; tl++ ) 
      for ( uint16_t c = 0; c < ap_Mtx->m_col; c++ ) 
      {
        for ( uint16_t l = 0; l < ap_Mtx->m_lin; l++ ) 
        {
          sum += GetMatriceElt(tl,l) * ap_Mtx->GetMatriceElt(l,c);
        }
        ap_MtxResult->SetMatriceElt(tl,c,sum);
        sum = 0;
      }
  }
  
  return 0;
}
